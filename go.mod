module github.com/devnw/atomizer-agent

go 1.14

require (
	github.com/devnw/alog v1.0.1
	github.com/devnw/atomizer v0.0.0-20200425014640-bf9d01a48c35
	github.com/devnw/conductors v0.0.0-20200425195205-f8a2c989296a
	github.com/devnw/montecarlopi v0.0.0-20200425201014-bca13b8f88d2
	github.com/pkg/errors v0.9.1
)
