# Atomizer Agent Instance

[![CI](https://github.com/devnw/atomizer-agent/workflows/CI/badge.svg)](https://github.com/devnw/atomizer-agent/actions)
[![Go Report Card](https://goreportcard.com/badge/github.com/devnw/atomizer-agent)](https://goreportcard.com/report/github.com/devnw/atomizer-agent)
[![codecov](https://codecov.io/gh/devnw/atomizer-agent/branch/master/graph/badge.svg)](https://codecov.io/gh/devnw/atomizer-agent)
[![GoDoc](https://godoc.org/github.com/devnw/atomizer-agent?status.svg)](https://pkg.go.dev/github.com/devnw/atomizer-agent)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg)](http://makeapullrequest.com)